package com.api.patientmanager.PatientManager.service;

import com.api.patientmanager.PatientManager.dto.PatientRequest;
import com.api.patientmanager.PatientManager.dto.PatientResponse;
import com.api.patientmanager.PatientManager.model.Sex;
import com.api.patientmanager.PatientManager.repository.PatientRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;


import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Testcontainers
@ContextConfiguration(initializers = {TestService.Initializer.class})
@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = {"com.api.patientmanager.PatientManager"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TestService {

    private static final MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:4.4.3").withReuse(true);

    @BeforeAll
    static void setUpAll() {
        mongoDBContainer.start();
    }

    @AfterAll
    static void tearDownAll() {
        mongoDBContainer.stop();
    }

    @Autowired
    private PatientRepository repository;

    @Autowired
    private PatientService service;

    @Test
    public void testCreatePatient() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();

        ResponseEntity response = service.createPatient(request);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testCreatePatientWithoutName() {
        PatientRequest request = PatientRequest.builder()
                .name(null)
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();
        ResponseEntity response = service.createPatient(request);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testGetAllPatients() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();
        PatientRequest request2 = PatientRequest.builder()
                .name("Lea")
                .birthdate("01/09/1980")
                .sex(Sex.F)
                .socialSecurityNumber("98765234567")
                .build();

        service.createPatient(request);
        service.createPatient(request2);
        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient(null);
        assertEquals(2, patients.getBody().size());
        assertEquals("John", patients.getBody().get(0).getName());
        assertEquals("Lea", patients.getBody().get(1).getName());
        assertEquals(HttpStatus.OK, patients.getStatusCode());
    }

    @Test
    public void testGetAllPatientsWithSSN() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();
        PatientRequest request2 = PatientRequest.builder()
                .name("Lea")
                .birthdate("01/09/1980")
                .sex(Sex.F)
                .socialSecurityNumber("98765234567")
                .build();
        PatientRequest request3 = PatientRequest.builder()
                .name("Lucas")
                .birthdate("01/10/1980")
                .sex(Sex.F)
                .socialSecurityNumber("98765234567")
                .build();

        service.createPatient(request);
        service.createPatient(request2);
        service.createPatient(request3);

        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient("98765234567");
        assertEquals(2, patients.getBody().size());
        assertEquals("Lea", patients.getBody().get(0).getName());
        assertEquals("Lucas", patients.getBody().get(1).getName());
        assertEquals(HttpStatus.OK, patients.getStatusCode());
    }

    @Test
    public void testGetAllPatientsWithSSNNotExist() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();

        service.createPatient(request);

        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient("1");
        assertEquals(HttpStatus.NOT_FOUND, patients.getStatusCode());
    }

    @Test
    public void testGetPatientById() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();
        PatientRequest request2 = PatientRequest.builder()
                .name("Lea")
                .birthdate("01/09/1980")
                .sex(Sex.F)
                .socialSecurityNumber("98765234567")
                .build();

        service.createPatient(request);
        service.createPatient(request2);

        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient(null);
        ResponseEntity<PatientResponse> result = service.getPatientById(patients.getBody().get(0).getId());
        assertEquals("John", result.getBody().getName());
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void testGetPatientByIdNotExist() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();
        PatientRequest request2 = PatientRequest.builder()
                .name("Lea")
                .birthdate("01/09/1980")
                .sex(Sex.F)
                .socialSecurityNumber("98765234567")
                .build();

        service.createPatient(request);
        service.createPatient(request2);

        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient(null);
        ResponseEntity<PatientResponse> result = service.getPatientById("1");
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    public void testUpdatePatient() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();

        service.createPatient(request);
        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient(null);
        PatientRequest request2 = PatientRequest.builder()
                .name("Lea")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();

        ResponseEntity<PatientResponse> result = service.updatePatientById(patients.getBody().get(0).getId(), request2);
        assertEquals("Lea", result.getBody().getName());
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void testUpdatePatientNotExist() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();

        service.createPatient(request);
        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient(null);
        PatientRequest request2 = PatientRequest.builder()
                .name("Lea")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();

        ResponseEntity<PatientResponse> result = service.updatePatientById("1", request2);
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    public void testDeletePatient() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();

        service.createPatient(request);
        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient(null);
        ResponseEntity response = service.deletePatientById(patients.getBody().get(0).getId());
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void testDeletePatientNotExist() {
        PatientRequest request = PatientRequest.builder()
                .name("John")
                .birthdate("17/03/1990")
                .sex(Sex.M)
                .socialSecurityNumber("1234567890")
                .build();

        service.createPatient(request);
        ResponseEntity<List<PatientResponse>> patients = service.getAllPatient(null);
        ResponseEntity response = service.deletePatientById("1");
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    String.format("spring.data.mongodb.uri=%s", mongoDBContainer.getReplicaSetUrl("my-deb" + UUID.randomUUID().getMostSignificantBits()))
            ).applyTo(configurableApplicationContext);
        }
    }
}
