package com.api.patientmanager.PatientManager.dto;

import com.api.patientmanager.PatientManager.model.Sex;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PatientResponse {
    private String id;
    private String name;
    private String birthdate;
    private Sex sex;
    private String socialSecurityNumber;
}
