package com.api.patientmanager.PatientManager.controller;

import com.api.patientmanager.PatientManager.dto.PatientRequest;
import com.api.patientmanager.PatientManager.dto.PatientResponse;
import com.api.patientmanager.PatientManager.service.PatientService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/patient-manager")
public class PatientController {
    private final PatientService service;

    @PostMapping("/anonymous")
    public ResponseEntity<Void> anonymous() {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/patients")
    @PreAuthorize("hasRole('User')")
    public ResponseEntity<Void> createPatient(@RequestBody PatientRequest request) {
        return service.createPatient(request);
    }

    @GetMapping("/patients?socialSecurityNumber={socialSecurityNumber}")
    public ResponseEntity<List<PatientResponse>> getAllPatient(@RequestParam(required = false) String socialSecurityNumber) {
        return service.getAllPatient(socialSecurityNumber);
    }

    @GetMapping("/patient/{id}")
    public ResponseEntity<PatientResponse> getPatientById(@PathVariable String id) {
        return service.getPatientById(id);
    }

    @PutMapping("/patient/{id}")
    public ResponseEntity<PatientResponse> updatePatientById(@PathVariable String id, @RequestBody PatientRequest request) {
        return service.updatePatientById(id, request);
    }

    @DeleteMapping("/patient/{id}")
    public ResponseEntity<Void> deletePatientById(@PathVariable String id) {
        return service.deletePatientById(id);
    }
}
