package com.api.patientmanager.PatientManager.service;

import com.api.patientmanager.PatientManager.dto.PatientRequest;
import com.api.patientmanager.PatientManager.dto.PatientResponse;
import com.api.patientmanager.PatientManager.model.Patient;
import com.api.patientmanager.PatientManager.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PatientService {
    @Autowired
    private PatientRepository repository;

    public ResponseEntity<Void> createPatient(PatientRequest request) {
        if(!(request.getName() != null && request.getBirthdate() != null && request.getSex() != null && request.getSocialSecurityNumber() != null)) {
            return ResponseEntity.badRequest().body(null);
        } else {
            Patient patient = Patient.builder()
                    .name(request.getName())
                    .birthdate(request.getBirthdate())
                    .sex(request.getSex())
                    .socialSecurityNumber(request.getSocialSecurityNumber())
                    .build();
            repository.save(patient);
            URI location = UriComponentsBuilder
                    .fromUriString("http://api/v1/stat-manager/patients?socialSecurityNumber={socialSecurityNumber}")
                    .build(patient.getSocialSecurityNumber());
            return ResponseEntity.created(location).build();
        }
    }

    public ResponseEntity<List<PatientResponse>> getAllPatient(String socialSecurityNumber) {
        List<Patient> patients;
        if(socialSecurityNumber != null) {
            patients = repository.findBySocialSecurityNumber(socialSecurityNumber);
        } else {
            patients = repository.findAll();
        }

        if(!patients.isEmpty()) {
            return ResponseEntity.ok(patients.stream().map(this::mapToPatientResponse).toList());
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    private PatientResponse mapToPatientResponse(Patient patient) {
        return PatientResponse.builder()
                .id(patient.getId())
                .name(patient.getName())
                .birthdate(patient.getBirthdate())
                .sex(patient.getSex())
                .socialSecurityNumber(patient.getSocialSecurityNumber())
                .build();
    }


    public ResponseEntity<PatientResponse> getPatientById(String id) {
        if(!repository.existsById(id)) {
            return ResponseEntity.notFound().build();
        } else {
            Patient patient = repository.findById(id).orElseThrow();

            return ResponseEntity.ok(mapToPatientResponse(patient));
        }
    }

    public ResponseEntity<PatientResponse> updatePatientById(String id, PatientRequest request) {
        if(!repository.existsById(id)) {
            return ResponseEntity.notFound().build();
        } else {
            Patient patient = repository.findById(id).orElseThrow();
            if (request.getName() != null) {
                patient.setName(request.getName());
            }

            if (request.getBirthdate() != null) {
                patient.setBirthdate(request.getBirthdate());
            }

            if (request.getSex() != null) {
                patient.setSex(request.getSex());
            }

            if (request.getSocialSecurityNumber() != null) {
                patient.setSocialSecurityNumber(request.getSocialSecurityNumber());
            }

            return ResponseEntity.ok(mapToPatientResponse(patient));
        }
    }

    public ResponseEntity<Void> deletePatientById(String id) {
        if(!repository.existsById(id)) {
            return ResponseEntity.notFound().build();
        } else {
            repository.deleteById(id);

            return ResponseEntity.noContent().build();
        }
    }

}