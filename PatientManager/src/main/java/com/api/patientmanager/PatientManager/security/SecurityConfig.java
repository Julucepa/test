package com.api.patientmanager.PatientManager.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.web.SecurityFilterChain;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeHttpRequests(registry -> {
            try {
                registry.requestMatchers(HttpMethod.GET, "/api/v1/patient-manager/anonymous").permitAll()
                        .requestMatchers(HttpMethod.POST, "/api/v1/patient-manager/patients").hasRole("user")
                        .requestMatchers(HttpMethod.GET, "/api/v1/patient-manager/patients?socialSecurityNumber={socialSecurityNumber}").hasRole("user")
                        .requestMatchers(HttpMethod.GET, "/api/v1/patient-manager/patient/{id}").hasRole("user")
                        .requestMatchers(HttpMethod.PUT, "/api/v1/patient-manager/patient/{id}").hasRole("user")
                        .requestMatchers(HttpMethod.DELETE, "/api/v1/patient-manager/patient/{id}").hasRole("user")
                        .anyRequest().authenticated();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).oauth2ResourceServer(oauth2Configurer -> oauth2Configurer
                .jwt(jwtConfigurer -> jwtConfigurer.jwtAuthenticationConverter(jwt -> {
                    Map<String, Collection<String>> realmAccess = jwt.getClaim("realm_access");
                    Collection<String> roles = realmAccess.get("roles");
                    List<SimpleGrantedAuthority> grantedAuthorities = roles.stream()
                            .map(role -> new SimpleGrantedAuthority("ROLE_" + role)).toList();
                    grantedAuthorities.forEach(e -> {
                        System.err.println(e.getAuthority());

                    });
                    return new JwtAuthenticationToken(jwt, grantedAuthorities);
                })));
        return httpSecurity.build();
    }
}

