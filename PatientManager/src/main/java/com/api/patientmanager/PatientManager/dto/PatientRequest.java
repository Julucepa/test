package com.api.patientmanager.PatientManager.dto;

import com.api.patientmanager.PatientManager.model.Sex;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientRequest {
    private String name;
    private String birthdate;
    private Sex sex;
    private String socialSecurityNumber;
}
