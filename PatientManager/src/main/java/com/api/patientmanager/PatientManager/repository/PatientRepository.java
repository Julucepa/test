package com.api.patientmanager.PatientManager.repository;

import com.api.patientmanager.PatientManager.model.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends MongoRepository<Patient, String> {
    public List<Patient> findBySocialSecurityNumber(String socialSecurityNumber);
}
